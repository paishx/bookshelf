import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, 
    StatusBar, SafeAreaView, ActivityIndicator, 
    Flatlist, Dimensions, Image, Modal, BackHandler, Alert } from 'react-native';
import data from "../../assets/data.json";
import { FlatList, TouchableWithoutFeedback, TouchableOpacity } from 'react-native-gesture-handler';
import Button from '../component/button';
import Header from '../component/header';

// view for flatlist
const Item = ({img, onPress,}) => {
    return(
        <View style={styles.itemStyle}>
            <TouchableOpacity onPress={onPress}>
                <Image source={img} />
            </TouchableOpacity>            
        </View>
    )
}
renderSeparatorView = () => {
    return(
        <View style={styles.container}>
            <View style={styles.separator}>

            </View>
        </View>
    )
}

export default class Shelf extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            dataSource: [],
            modalVisible : false,
        };

    }

    componentDidMount() {
        this.setState({isLoading : false, dataSource: data})

    }

    // set the item into state for the book title and cover
    setItem = (visible, title, cover) => {
        console.log(cover)
        this.setState({title: title})
        this.setState({cover: cover})
        this.setState({modalVisible: visible})
    }

    

    render() {
        // loading bar
        if(this.state.isLoading) {
            return(
                <View style={styles.container}>
                    <ActivityIndicator />
                </View>
            )
        }
        const {modalVisible} = this.state
        return (
            <SafeAreaView style={{flex : 1}}>
                <Header title='App Name' isHome={true} />
                <View style={styles.container}>
                    <ImageBackground source={require('../../assets/background.png')} style={styles.img} >

                    <View>
                        {/* modal start here */}
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={modalVisible}
                            onRequestClose={() => {
                                this.setState({modalVisible: false})
                              }}
                        >
                           <View style={styles.container}>
                                <View style={styles.modalView}>
                                    <View style={styles.modalHorizontal}>
                                        <Image source={require("../../assets/cover1.png")} />
                                        <Text style={styles.titleModal}>{this.state.title}</Text>
                                    </View>
                                    
                                    <Button word='READ' color='#27A0ED' />
                                    <Button word='SHARE' color='#27A0ED'/>
                                    <Button word='DELETE' color='#FF0000'/>
                                </View>
                           </View>

                        </Modal>
                        {/* modal end here */}

                    </View>
                    {/* flatlist to show the list item from data.json */}
                        <FlatList 
                            data={this.state.dataSource}
                            numColumns={2}
                            ItemSeparatorComponent={renderSeparatorView}
                            renderItem={({item}) => <Item img={require("../../assets/cover4.png")}  onPress={() => {this.setItem(true, item.title, item.cover)}}/>}
                            keyExtractor={(item,index) => index.toString()}
                        />
                    </ImageBackground>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex : 1,
        justifyContent: 'center',
        alignItems : 'center'
    },
    img : {
        flex : 1,
        width : '100%'
    },
    itemStyle : {
        flex : 1,
        alignItems: 'center',
        justifyContent: 'center',
        width : '100%',
        marginTop: 20,
        marginBottom : -30,
    },
    separator : {
        flex : 1,
        height: 20, 
        width: '95%',
        backgroundColor: '#B47326',
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
        shadowOffset: { width: 10, height: 10 },
        shadowColor: 'black',
        shadowOpacity: 1,
        elevation: 10,        
    },
    modalView : {
        height: '85%',
        width: '90%',
        marginTop : 60,
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor: '#D6D6D6',
        borderRadius: 15,
        borderWidth: 1,
        borderColor : 'white',
        shadowOffset: { width: 10, height: 10 },
        shadowColor: 'black',
        shadowOpacity: 1,
        elevation: 10,   
    },
    modalHorizontal : {
        width: '90%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent :'space-between',
        alignItems: 'flex-start'
    },
    titleModal : {
        marginRight: 10, 
        width: '45%',
        fontWeight : "700",
        fontSize: 19,
    }
    
})