import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default function Header({ onPress, title, isHome}) {
    
    return (
        <SafeAreaView>
            <View style={styles.header}>
                
                <View style={styles.headerColumn}>
                    <Text style={styles.headerText}>{title}</Text>
                </View>
            </View>
        </SafeAreaView>
    )
} 


const styles = StyleSheet.create({
    header : {
        height : 50,
        flexDirection : 'row',
        width: '100%',
        backgroundColor: '#435254',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 24,
    },
    headerColumn : {
        flex : 1, 
        justifyContent : 'center',
        alignItems: 'flex-start',
        marginLeft: 10,
        
    },
    headerText : {
        fontSize : 16,
        color: 'white'
    }
})