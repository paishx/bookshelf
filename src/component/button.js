import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

export default function Button({onPress, word, color}) {
 
    return (
      <View>
        <TouchableOpacity onPress={onPress}>
            <View style={[styles.background, {backgroundColor: color}]}>
                <Text style={{color: 'white'}}>{word}</Text>
            </View>
        </TouchableOpacity>
      </View>
    );
  
}

const styles = StyleSheet.create({
    background : {
        justifyContent: 'center',
        alignItems: 'center',
        width: 280,
        height : 50,
        borderRadius: 10,
        borderTopWidth: 1,
        borderTopColor: 'white',
        marginVertical: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 20,
        overflow: 'hidden',
    }
})
