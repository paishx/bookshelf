import 'react-native-gesture-handler';
import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import BookShelfScreen from "../screen/shelf"

const Stack = createStackNavigator();

const navOptionHandler = () => ({
    headerShown: false,
  }); 

export default class AppNavigator extends React.Component {
    render() {
        
        return(
            <NavigationContainer>
                <Stack.Navigator initialRouteName="BookShelf">
                    <Stack.Screen 
                        name="BookShelf"
                        component={BookShelfScreen}
                        options={navOptionHandler}

                    />
                </Stack.Navigator>
            </NavigationContainer>
        )
    }
}